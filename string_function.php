<?php
//addslashes
$str = addslashes('Where are you come from?');
echo($str) ."<hr>";

//explode
$str = "How old are you?";
print_r (explode(" ",$str)) ;
echo "<hr>";

//implode
$arr = array('Everything','is','possible');
echo implode(" ",$arr);
echo "<hr>";

//str_shuffle
echo str_shuffle("Hello Everybody");
echo "<hr>";

//printf
$bottle = 9;
$str = "Glass";
printf("There are %u bottle and four %s.",$bottle,$str);
echo "<hr>";

//trim
$str = "Good";
echo $str . "<br>";
echo trim($str,"Go!");
echo "<hr>";

//ltrim

$str = "Good Morning!";
echo $str . "<br>";
echo ltrim($str,"Good");
echo "<hr>";

//rtrim
$str = "Good Morning!";
echo $str . "<br>";
echo rtrim($str,"Morning!");
echo "<hr>";

//nl2br
echo nl2br("Good Morning.\nGood Night");
echo "<hr>";

//str_pad
$str = "Good Morning";
echo str_pad($str,15,"*");
echo "<hr>";

//str_repeat

echo str_repeat("Good",10);
echo "<hr>";

//str_replace
echo str_replace("World","PHP","Hellow World!");
echo "<hr>";

//str_split
print_r(str_split("Good"));
echo "<hr>";

//strip _tags
echo strip_tags("Good  <h4>Morning!</h4>");
echo "<hr>";

//strlen
echo strlen("Good Morning");
echo "<hr>";

//strtoupper
echo strtoupper("Good Morning!") ."<hr>";


//strtolower
echo strtolower("Good Morning!") ."<hr>";

//substr
echo substr("Good Morning",4) ."<hr>";

//substr_count
echo substr_count("My name is Aharna Sen.MY Village name is.....","name");
echo "<hr>";

//ucwords
echo ucwords("Good Morning") ."<hr>";

//ucfirst
echo ucfirst("BASIS Institute of Technology And Management ");
?>


