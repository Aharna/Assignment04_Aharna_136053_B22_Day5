<?php
//AND
$a = 100;
$b = 50;
if ($a == 100 and $b == 50) {
    echo "AND";
}
echo "<hr>";

//OR
$c = 100;
$d = 50;

if ($c == 100 or $d == 80) {
    echo "OR";
}
echo "<hr>";

//XOR
$e = 100;
$f = 50;

if ($e == 100 or $f == 80) {
    echo "XOR";
}
echo "<hr>";



//NOT
$g = 100;

if ($g !== 90) {
    echo "NOT";
}
echo "<hr>";

//AND
$h = 100;
$i = 50;

if ($h == 100 && $i == 50) {
    echo "AND";
}
echo "<hr>";

//OR

$j = 100;
$k = 50;

if ($j == 100 || $k == 80) {
    echo "OR";
}

