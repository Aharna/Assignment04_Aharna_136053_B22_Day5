<?php
//equal
$a=20;
$b=20;
var_dump($a==$b);
echo"<hr>";

//not equal
$c=100;
$d=30;
var_dump($c!=$d);
echo "<hr>";

//identical
$e=25;
$f="30";
var_dump($e===$f);
echo "<hr>";

//not identical
$g="50";
$h=10;
var_dump($g!==$h);
echo "<hr>";

//greater than
$i=30;
$j=20;
var_dump($i>$j);
echo "<hr>";

//less than
$k=33;
$l=53;
var_dump($k<$l);
echo "<hr>";

//greater than equal
$m=35;
$n=35;
var_dump($m>=$n);
echo "<hr>";

//less than equal
$o=25;
$p=25;
var_dump($o<=$p);
echo "<hr>";

?>