<?php
$a=30;
$b=40;

//post increment
echo $a++;
echo "<br>";
echo $b++;
echo "<hr>";

//pre increment
$e=50;
$f=20;
echo ++$e;
echo "<br>";
echo ++$f;
echo "<hr>";

//post decrement
$g=10;
$h=15;
echo $g--;
echo "<br>";
echo $h--;
echo "<hr>";

//pre decrement
$i=44;
$j=17;
echo --$i;
echo "<br>";
echo --$j;
echo "<hr>";


?>

